import { NgModule } from '@angular/core';
import { MatButtonModule,
          MatGridListModule,
          MatIconModule,
          MatInputModule,
          MatListModule,
          MatSidenavModule,
          MatTabsModule,
          MatToolbarModule,
          MatTableModule,
          MatSelectModule,
          MatCheckboxModule,
          MatCardModule,
          MatChipsModule } from '@angular/material';

const materialModules = [
  MatButtonModule,
  MatInputModule,
  MatToolbarModule,
  MatListModule,
  MatIconModule,
  MatSidenavModule,
  MatTabsModule,
  MatGridListModule,
  MatTableModule,
  MatSelectModule,
  MatCheckboxModule,
  MatCardModule,
  MatChipsModule
];
@NgModule({
  declarations: [],
  imports: [
    materialModules
  ],
  exports: [
    materialModules
  ]
})
export class MaterialModule { }
