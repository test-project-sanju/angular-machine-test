import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';
@Pipe({
    name: 'age'
})
export class AgePipe implements PipeTransform {

  transform(value: Date, ageOnly?): any {
    const today = moment();
    const birthdate = moment(value);
    const years = today.diff(birthdate, 'years');
    const age: string = years > 1 ? years + ' years' : years + ' year';
    if (ageOnly) {
      return years;
    }
    return age;
  }
}
