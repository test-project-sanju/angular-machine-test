
import { of as observableOf, Observable } from 'rxjs';

import { catchError, map } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { CanActivate } from '@angular/router';

@Injectable()
export class AuthGuard implements CanActivate {

    constructor(private router: Router) { }

    canActivate() {
        const authSettings = JSON.parse(localStorage.getItem('authSettings'));
        if (authSettings && (authSettings.arrayTotal === authSettings.inputTotal)) {
            return true;
        } else {
            window.alert ('Not authorized');
            return false;
        }
    }
}
