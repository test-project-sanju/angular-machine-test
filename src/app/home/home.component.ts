import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  public arr: number[] = [];
  public arrInput: number = null;
  public arrTotal: number = null;

  constructor() { }

  ngOnInit() {
  }

  addToArray() {
    if (this.arrInput === null || this.arr.length === 5) {
      return;
    }
    this.arr.push(this.arrInput);
    this.arrInput = null;
  }

  resetArray() {
    this.arr = [];
    this.arrInput = null;
    this.arrTotal = null;
  }

  submit() {
    if (this.arr.length < 5 || this.arrTotal == null) {
      alert('Please enter all inputs!');
      return;
    }
    try {
      const sum = this.arr.reduce((accumulator, currentValue) => {
        return accumulator + currentValue;
      }, 0);
      const authSum = {
        arrayTotal: sum,
        inputTotal: this.arrTotal
      };
      localStorage.removeItem('authSettings');
      localStorage.setItem('authSettings', JSON.stringify(authSum) );
      this.arrInput = null;
      this.arrTotal = null;
      this.arr = [];
      alert('Values entered succesfully!');
    } catch (e) {
      alert('Some error occurred!');
    }
  }

}
