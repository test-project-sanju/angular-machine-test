import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { UserService } from 'src/app/services/user.service';
import { AgePipe } from './../../pipes/age.pipe';
export interface UserDetails {
  select: boolean;
  name: string;
  id: number;
  dob: string;
  phone: string;

}

@Component({
  selector: 'app-user-listing',
  templateUrl: './user-listing.component.html',
  styleUrls: ['./user-listing.component.scss']
})
export class UserListingComponent implements OnInit {
  displayedColumns: string[] = ['checked', 'id', 'name', 'dob', 'phone'];
  dataSource: any;
  dataSourceOriginal: any;
  loading = true;
  selectedUsers = [];
  selectedFilter = '';

  private jsonURL = 'assets/users.json';

  constructor(
    private http: HttpClient,
    private userService: UserService,
    private agePipe: AgePipe
    ) { }

  ngOnInit() {
    this.getJSON().subscribe(data => {
      this.dataSource = data;
      this.dataSourceOriginal = data;
      setTimeout(() => {
        this.loading = false;
      }, 2000);
     });
  }
  public getJSON(): Observable<any> {
    return this.http.get(this.jsonURL);
  }
  selectUser(user) {
    let hasUser = false;
    let selectedindex = -1;
    this.selectedUsers.forEach((data, index) => {
      if (data.id === user.id) {
        hasUser = true;
        selectedindex = index;
      }
    });
    if (user.checked && !hasUser)  {
        this.selectedUsers.push(user);
    } else if (!user.checked && hasUser) {
      this.selectedUsers.splice(selectedindex, 1);
    }
  }
  filterFields() {
    switch (this.selectedFilter) {
      case '18': {
        this.dataSource = this.dataSourceOriginal.filter((data, index) =>
          this.agePipe.transform(data.dob, true) <= 18 );
        break;
      }
      case '18-56': {
        this.dataSource = this.dataSourceOriginal.filter((data, index) =>
          (this.agePipe.transform(data.dob, true) > 18) && (this.agePipe.transform(data.dob, true) < 56 ));
        break;
      }
      case '56': {
        this.dataSource = this.dataSourceOriginal.filter((data, index) =>
          this.agePipe.transform(data.dob, true) > 56 );
        break;
      }
      case 'all': {
        this.dataSource = this.dataSourceOriginal.filter((data, index) =>
          this.agePipe.transform(data.dob, true) >= 0 );
        break;
      }
      default:
        break;
    }
  }
  submit() {
    this.userService.userSubject.next({data: JSON.parse(JSON.stringify(this.selectedUsers))});
    window.scrollTo(0, 0);
  }
}
