import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserListingComponent } from './user-listing/user-listing.component';
import { UserDetailsComponent } from './user-details/user-details.component';
import { UserHomeComponent } from './user-home/user-home.component';
import { UserRoutingModule } from './user-routing-module';
import { MaterialModule } from './../material/material.module';
import { HttpClientModule } from '@angular/common/http';
import { AgePipe } from './../pipes/age.pipe';
import { ngxLoadingAnimationTypes, NgxLoadingModule } from 'ngx-loading';
import { FormsModule } from '@angular/forms';
import { UserService } from '../services/user.service';

@NgModule({
  declarations: [
    UserListingComponent,
    UserDetailsComponent,
    UserHomeComponent,
    AgePipe],
  imports: [
    FormsModule,
    CommonModule,
    UserRoutingModule,
    MaterialModule,
    HttpClientModule,
    NgxLoadingModule.forRoot({
        animationType: ngxLoadingAnimationTypes.wanderingCubes,
        backdropBackgroundColour: 'rgba(0,0,0,0.1)',
        backdropBorderRadius: '4px',
        primaryColour: '#ffffff',
        secondaryColour: '#ffffff',
        tertiaryColour: '#ffffff'
    })
  ],
  providers: [
    UserService,
    AgePipe
  ]
})
export class UserManagementModule { }
