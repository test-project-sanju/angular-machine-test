import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.scss']
})
export class UserDetailsComponent implements OnInit,OnDestroy {
  private userSubscription: Subscription;
  userData: any;

  constructor(private userService: UserService) { }

  ngOnInit() {
    this.userSubscription = this.userService.userSubject.subscribe(data => {
      this.userData = data.data;
    });
  }
  ngOnDestroy() {
    this.userSubscription.unsubscribe();
  }
}
