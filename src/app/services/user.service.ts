import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';


@Injectable()
export class UserService {
  public userSubject: Subject<any> = new Subject();
  constructor() { }
}
