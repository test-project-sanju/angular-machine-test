import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChartsComponent } from './charts/charts.component';
import { AdminPanelComponent } from './admin-panel/admin-panel.component';
import { AdminRoutingModule } from './admin-routing.module';
import { ChartsModule, ThemeService } from 'ng2-charts';

@NgModule({
  declarations: [
    ChartsComponent,
    AdminPanelComponent],
  imports: [
    CommonModule,
    AdminRoutingModule,
    ChartsModule,
  ],
  providers:[
    ThemeService
  ]
})
export class AdminModule { }
